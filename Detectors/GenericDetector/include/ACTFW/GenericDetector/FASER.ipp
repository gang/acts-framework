
int nPlanes = 10;
double ModuleLengthX = 124*Acts::units::_mm;
double ModuleLengthY = 62*Acts::units::_mm;
// For SCT, the equivalent pitch are pitch/(sqrt(2)*sin(stereo angle)) and pitch/(sqrt(2)*cos(stereo angle))
//int ReadoutBinsX =44;
int ReadoutBinsX =2;
int ReadoutBinsY =1107;
// Number of modules in each direction
int ModuleBinsX = 2;
int ModuleBinsY = 4;
int ReadoutSide = 1;
double LorentzAngle =0 ;
/*
Acts::Vector3D trackerDimensions = { 5000.0 * Acts::units::_mm, 400.0 * Acts::units::_mm, 400.0 * Acts::units::_mm };
Acts::Vector3D trackerCenter = {2500.0 * Acts::units::_mm, 0.0, 0.0};
Acts::Vector3D worldDimensions = { 1000.0 * Acts::units::_mm, 1000.0 * Acts::units::_mm, 1000.0 * Acts::units::_mm };
Acts::Vector3D worldCenter = {0.0, 0.0, 0.0};
*/
Acts::Vector3D trackerDimensions = { 5000.0 * Acts::units::_mm, 400.0 * Acts::units::_mm, 400.0 * Acts::units::_mm };
Acts::Vector3D trackerCenter = {0.0 * Acts::units::_mm, 0.0, 0.0};
Acts::Vector3D worldDimensions = { 5000.0 * Acts::units::_mm, 400.0 * Acts::units::_mm, 400.0 * Acts::units::_mm };
Acts::Vector3D worldCenter = {0.0, 0.0, 0.0};

std::vector<Acts::Vector3D> planePos = {
	/*
  {-996.0*Acts::units::_mm, 0., 0.},
  {-946.0*Acts::units::_mm, 0., 0.},
  {-896.0*Acts::units::_mm, 0., 0.},
  {-50.0*Acts::units::_mm, 0., 0.},
  {0.0*Acts::units::_mm, 0., 0.},
  {50.0*Acts::units::_mm, 0., 0.},
  {896.0*Acts::units::_mm, 0., 0.},
  {946.0*Acts::units::_mm, 0., 0.},
  {996.0*Acts::units::_mm, 0., 0.}
  */
	
  {-100.0*Acts::units::_mm, 0., 0.},
  {200.0*Acts::units::_mm, 0., 0.},
  {250.0*Acts::units::_mm, 0., 0.},
  {300.0*Acts::units::_mm, 0., 0.},
  {1146.0*Acts::units::_mm, 0., 0.},
  {1196.0*Acts::units::_mm, 0., 0.},
  {1246.0*Acts::units::_mm, 0., 0.},
  {2092.0*Acts::units::_mm, 0., 0.},
  {2142.0*Acts::units::_mm, 0., 0.},
  {2192.0*Acts::units::_mm, 0., 0.}
  
	/*
  {-986.0*Acts::units::_mm, 0., 0.},
  {-936.0*Acts::units::_mm, 0., 0.},
  {-886.0*Acts::units::_mm, 0., 0.},
  {-40.0*Acts::units::_mm, 0., 0.},
  {10.0*Acts::units::_mm, 0., 0.},
  {60.0*Acts::units::_mm, 0., 0.},
  {906.0*Acts::units::_mm, 0., 0.},
  {956.0*Acts::units::_mm, 0., 0.},
  {1006.0*Acts::units::_mm, 0., 0.}
  */
};


//------------------------------------------------------------------------------

  std::vector<Acts::CuboidVolumeBuilder::LayerConfig> layerConfigs;

  for (uint iPlane=0; iPlane<nPlanes; ++iPlane) {
    std::vector<std::shared_ptr<const Acts::Surface>> surVect;
    // Position of the surfaces
    Acts::CuboidVolumeBuilder::SurfaceConfig cfg;
    cfg.position = planePos[iPlane];
    cfg.rBounds = std::make_shared<const Acts::RectangleBounds>(
        Acts::RectangleBounds (0.5*ModuleLengthX, 0.5*ModuleLengthY) );
    //cfg.thickness = 1. * Acts::units::_mm;
    cfg.thickness = 30. * Acts::units::_um;

    Acts::Vector3D xPos(0., 0., 1.);
    Acts::Vector3D yPos(0., 1., 0.);
    Acts::Vector3D zPos(-1., 0., 0.);
    Acts::RotationMatrix3D xzRot;
    xzRot.col(0) = xPos;
    xzRot.col(1) = yPos;
    xzRot.col(2) = zPos;
    cfg.rotation = xzRot; 

    // Boundaries of the surfaces
    std::shared_ptr<const Acts::PlanarBounds> moduleBounds(
		              new Acts::RectangleBounds(0.5*ModuleLengthX, 0.5*ModuleLengthY));

    // Material of the surfaces (X0, L0, A, Z, Rho, thickness)
    // TODO: Update these material properties (currently using default values)
    Acts::MaterialProperties matProp(95.7, 465.2, 28.03, 14., 2.32e-3, 2. * Acts::units::_mm);
    cfg.surMat = std::shared_ptr<const Acts::SurfaceMaterial>(
        new Acts::HomogeneousSurfaceMaterial(matProp));

    // create digitizaiton module
    std::shared_ptr<const Acts::DigitizationModule> moduleDigitizationPtr = nullptr;
    // create the CartesianSegmentation
    std::shared_ptr<const Acts::Segmentation> moduleSegmentation
            = std::make_shared<const Acts::CartesianSegmentation>(
                moduleBounds,
                ReadoutBinsX,
                ReadoutBinsY);
    // now create the digitzation module
    moduleDigitizationPtr
            = std::make_shared<const Acts::DigitizationModule>(
                moduleSegmentation,
                cfg.thickness,
                ReadoutSide,
                LorentzAngle);

    int iModule = 0;
    for (int iModuleX = 0; iModuleX < ModuleBinsX; ++iModuleX )
     for (int iModuleY = 0; iModuleY < ModuleBinsY; ++iModuleY )
      {
	++iModule;

        Identifier moduleIdentifier = Identifier(Identifier::identifier_type(iModule));

	double GlobalX = planePos[iPlane].x();
	double GlobalY = (iModuleY - ModuleBinsY/2.0 + 0.5)*ModuleLengthY;
	double GlobalZ = (iModuleX - ModuleBinsX/2.0 + 0.5)*ModuleLengthX;
	Acts::Vector3D ModulePosition = {GlobalX, GlobalY, GlobalZ};

        // get the moduleTransform
        std::shared_ptr<Acts::Transform3D> mutableModuleTransform
            = std::make_shared<Acts::Transform3D>(
                Acts::Translation3D(ModulePosition) * cfg.rotation);
        auto moduleTransform = std::const_pointer_cast<const Acts::Transform3D>(
            mutableModuleTransform);

        // create the module
        Acts::DetectorElementBase* module
            = new FW::Generic::GenericDetectorElement(moduleIdentifier,
                                                      moduleTransform,
                                                      moduleBounds,
                                                      cfg.thickness,
                                                      cfg.surMat,
                                                      moduleDigitizationPtr);

	surVect.push_back(module->surface().getSharedPtr());
      }
    
    Acts::CuboidVolumeBuilder::LayerConfig layercfg;
    layercfg.surfaceCfg = cfg;
    layercfg.surface = surVect;
    layercfg.active = true;
    layerConfigs.push_back(layercfg);
  }

  Acts::CuboidVolumeBuilder::VolumeConfig volumeConfig;
  volumeConfig.position = trackerCenter;
  volumeConfig.length   = trackerDimensions;
  volumeConfig.layerCfg = layerConfigs;
  volumeConfig.name     = "FASER";

  Acts::CuboidVolumeBuilder::Config config;
  //config.position  = trackerCenter;
  //config.length    = trackerDimensions;
  config.position  = worldCenter;
  config.length    = worldDimensions;
  config.volumeCfg = {volumeConfig};

  auto cvb = std::make_shared<const Acts::CuboidVolumeBuilder>(config);

  // add to the list of builders
  volumeBuilders.push_back(cvb);

