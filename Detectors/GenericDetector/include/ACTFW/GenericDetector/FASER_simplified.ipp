
int nPlanes = 9;
double width = 20*Acts::units::_cm;
double length = 20*Acts::units::_cm;
//Acts::Vector3D  trackerDimensions = { 400.0 * Acts::units::_mm, 400.0 * Acts::units::_mm, 4000.0 * Acts::units::_mm };
Acts::Vector3D  trackerDimensions = { 4000.0 * Acts::units::_mm, 400.0 * Acts::units::_mm, 400.0 * Acts::units::_mm };
Acts::Vector3D trackerCenter = {0.0, 0.0, 0.0 * Acts::units::_mm};

std::vector<Acts::Vector3D> planePos = {
  /*
  {0., 0., -996.0*Acts::units::_mm},
  {0., 0., -946.0*Acts::units::_mm},
  {0., 0., -896.0*Acts::units::_mm},
  {0., 0., -50.0*Acts::units::_mm},
  {0., 0., 0.0*Acts::units::_mm},
  {0., 0., 50.0*Acts::units::_mm},
  {0., 0., 896.0*Acts::units::_mm},
  {0., 0., 946.0*Acts::units::_mm},
  {0., 0., 996.0*Acts::units::_mm}
  */
  {-996.0*Acts::units::_mm, 0., 0.},
  {-946.0*Acts::units::_mm, 0., 0.},
  {-896.0*Acts::units::_mm, 0., 0.},
  {-50.0*Acts::units::_mm, 0., 0.},
  {0.0*Acts::units::_mm, 0., 0.},
  {50.0*Acts::units::_mm, 0., 0.},
  {896.0*Acts::units::_mm, 0., 0.},
  {946.0*Acts::units::_mm, 0., 0.},
  {996.0*Acts::units::_mm, 0., 0.}
};


//------------------------------------------------------------------------------
  int ReadoutBinsX =10;
  int ReadoutBinsY =10;

  std::vector<Acts::CuboidVolumeBuilder::SurfaceConfig> surfaceConfigs;
  std::vector<Acts::CuboidVolumeBuilder::LayerConfig> layerConfigs;

  for (uint iPlane=0; iPlane<nPlanes; ++iPlane) {
    // Position of the surfaces
    Acts::CuboidVolumeBuilder::SurfaceConfig cfg;
    cfg.position = planePos[iPlane];

    Acts::Vector3D xPos(0., 0., 1.);
    Acts::Vector3D yPos(0., 1., 0.);
    Acts::Vector3D zPos(-1., 0., 0.);
    Acts::RotationMatrix3D xzRot;
    xzRot.col(0) = xPos;
    xzRot.col(1) = yPos;
    xzRot.col(2) = zPos;
    cfg.rotation = xzRot; 

    // Boundaries of the surfaces
    cfg.rBounds = std::make_shared<const Acts::RectangleBounds>(
        Acts::RectangleBounds (0.5*width, 0.5*length) );

    std::shared_ptr<const Acts::PlanarBounds> moduleBounds(
		              new Acts::RectangleBounds(0.5*width, 0.5*length));

    // Material of the surfaces
    // TODO: Update these material properties (currently using default values)
    Acts::MaterialProperties matProp(
        352.8, 407., 9.012, 4., 1.848e-3, 0.5 * Acts::units::_mm);
    cfg.surMat = std::shared_ptr<const Acts::SurfaceMaterial>(
        new Acts::HomogeneousSurfaceMaterial(matProp));

    // Thickness of the detector element
    cfg.thickness = 1. * Acts::units::_mm;

      // create digitizaiton module
      std::shared_ptr<const Acts::DigitizationModule> moduleDigitizationPtr
          = nullptr;
        // create the CartesianSegmentation
        std::shared_ptr<const Acts::Segmentation> moduleSegmentation
            = std::make_shared<const Acts::CartesianSegmentation>(
                moduleBounds,
                ReadoutBinsX,
                ReadoutBinsY);
        // now create the digitzation module
        moduleDigitizationPtr
            = std::make_shared<const Acts::DigitizationModule>(
                moduleSegmentation,
                cfg.thickness,
                -1,
                0);

        Identifier moduleIdentifier
            = Identifier(Identifier::identifier_type(iPlane+1));

        // get the moduleTransform
        std::shared_ptr<Acts::Transform3D> mutableModuleTransform
            = std::make_shared<Acts::Transform3D>(
                Acts::Translation3D(cfg.position) * cfg.rotation);
        auto moduleTransform = std::const_pointer_cast<const Acts::Transform3D>(
            mutableModuleTransform);
        // create the module
        Acts::DetectorElementBase* module
            = new FW::Generic::GenericDetectorElement(moduleIdentifier,
                                                      moduleTransform,
                                                      moduleBounds,
                                                      cfg.thickness,
                                                      cfg.surMat,
                                                      moduleDigitizationPtr);
    
	surfaceConfigs.push_back(cfg);
    Acts::CuboidVolumeBuilder::LayerConfig layercfg;
    layercfg.surfaceCfg = cfg;
    layercfg.surface = module->surface().getSharedPtr();
    layercfg.active = true;
    layerConfigs.push_back(layercfg);
  }
/*
  std::vector<Acts::CuboidVolumeBuilder::LayerConfig> layerConfigs;
  for (auto& sCfg : surfaceConfigs) {
    Acts::CuboidVolumeBuilder::LayerConfig cfg;
    cfg.surfaceCfg = sCfg;
    cfg.active = true;
    layerConfigs.push_back(cfg);
  }
*/
  Acts::CuboidVolumeBuilder::VolumeConfig volumeConfig;
  volumeConfig.position = trackerCenter;
  volumeConfig.length   = trackerDimensions;
  volumeConfig.layerCfg = layerConfigs;
  volumeConfig.name     = "FASER";

  Acts::CuboidVolumeBuilder::Config config;
  config.position  = trackerCenter;
  config.length    = trackerDimensions;
  config.volumeCfg = {volumeConfig};

  //cvb.setConfig(config);
auto cvb = std::make_shared<const Acts::CuboidVolumeBuilder>(config);

// add to the list of builders
volumeBuilders.push_back(cvb);
/*
  TrackingGeometryBuilder::Config tgbCfg;
  tgbCfg.trackingVolumeBuilders.push_back(
      std::make_shared<const CuboidVolumeBuilder>(cvb));
  TrackingGeometryBuilder tgb(tgbCfg);

  detector = std::move(tgb.trackingGeometry());
*/



